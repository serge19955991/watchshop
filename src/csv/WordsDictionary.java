package csv;

import java.io.*;
import java.util.*;

/**
 * Created by Сергей on 04.10.2016.
 */
public class WordsDictionary {
    TreeMap<String, Integer> dictionary = new TreeMap<>();
    ArrayList<String> words = new ArrayList<>();
    List sorted;
    public void _import(String file) {
        Reader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(file));
            int i;
            StringBuilder oneword = new StringBuilder();
            reader = new InputStreamReader(new FileInputStream(file));
            while ((i = reader.read()) != -1) {
                if (Character.isLetterOrDigit(i))
                    oneword.append((char) i);
                else {
                    if (!oneword.toString().equals("")) {
                        words.add(new   String(oneword));
                        oneword = new StringBuilder();
                    }
                }

            }
            if (oneword.toString().equals(""))
                words.add(new String(oneword));
        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }

    }


    public void print() {
        System.out.print(sorted);
    }


    public void wordscount()
    {
        for (String oneword : words){
            if (this.dictionary.containsKey(oneword))  {
                Integer n  =  this.dictionary.get(oneword);
                n ++;
                this.dictionary.put(oneword,n);

            } else {
                dictionary.put(oneword,1);
            }

        }

    }
    public void createsortList(){
        sorted = new LinkedList<>(dictionary.entrySet());
        Comparator comp = new Comparator() {
            public int compare(Object o1, Object o2) {
                return -(     (Comparable) (    ( Map.Entry)o1   ).getValue()       )
                        .compareTo(        (   (Map.Entry) o2    ).getValue()    );
            }
        };

        Collections.sort(sorted, comp);
    }
    public void export(String file){
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            Iterator i = sorted.iterator();
            while ( i.hasNext() ) {
                Map.Entry<String,Integer> z = (Map.Entry)i.next();
                out.println(z.getKey()+","+z.getValue()+","+z.getValue()*100/(words.size()));
            }
        } catch (IOException e){}
    }
}



