package sipatr;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Сергей on 28.09.2016.
 */
public class Shop {
    Set<Watch> watches = new HashSet<>();
    Set<Watch> quartz = new HashSet<>();
    Set<Watch> mechanical = new HashSet<>();
    HashMap<Manufacturer,Float> CushManufacturerPrices = new HashMap<>();

    public void addWatches(Watch thewatch) {
        watches.add(thewatch);
        if (thewatch.type == Watch.Type.quartz) {
            quartz.add(thewatch);
        } else
            mechanical.add(thewatch);
        if (CushManufacturerPrices.containsKey(thewatch.manufacturer))
        {
            float z = CushManufacturerPrices.remove(thewatch.manufacturer);
            z += thewatch.price;
            CushManufacturerPrices.put(thewatch.manufacturer,z);
        }
        else  {
            CushManufacturerPrices.put(thewatch.manufacturer,thewatch.price);
        }
    }

    public void printmarks(Watch.Type thetype) {
        if (thetype == Watch.Type.quartz) {
            System.out.println("Quartz : ");
            for (Watch thewatch : quartz) {
                System.out.println(thewatch.mark);
            }
        } else {
            System.out.println("Mechanical : ");
            for (Watch thewatch : mechanical) {
                System.out.println(thewatch.mark);
            }
        }
    }
    public void printMechPriceLess(float price){
        System.out.println("\n\nInformation about mech price less then " + price);
        for (Watch thewatch:mechanical)
        {
             if (thewatch.price <=  price)
             {
                 System.out.println(thewatch);
             }
        }
    }
    public void printWatchMarkInSomeCountry(String country)
    {
        System.out.println("\n\nPrinting country watches  mark's  " + country);
        for (Watch thewatch:watches)
              {
                  if (thewatch.manufacturer.country.hashCode() == country.hashCode())
                  {
                      System.out.println(thewatch.mark);
                  }
              }
    }
    public void printManufaturerSum(float price){
        System.out.println("\n\nPrinting manufacturer that agree with task,total price less than :  " + price);
        for (Map.Entry<Manufacturer,Float> pair :CushManufacturerPrices.entrySet())
        {
            if (pair.getValue() < price)
                System.out.println(pair.getKey());
        }
    }
    public void export (String file){
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            for (Watch thewatch: watches){
               String e = thewatch.mark + "," + thewatch.type + "," + thewatch.price + ","+ thewatch.count +"," + thewatch.manufacturer.country +"," + thewatch.manufacturer.name ;
                out.println(e);

            }
        } catch (IOException e){}
    }
    public void _import (String file){
        Reader reader = null;
        try
        {
            int i;
            String data = "";
            reader = new InputStreamReader(new FileInputStream(file));
            while ( (i=reader.read()) != -1)
                data += (char)i;

            String[] snowmass = data.split("\r\n");
            for (String variable:snowmass
                 ) {
                String[] ggplot2 = variable.split(",");
                String mark = ggplot2[0];
                Watch.Type t;
                if (ggplot2[1].hashCode() == "quartz".hashCode())
                    t = Watch.Type.quartz;
                else
                    t = Watch.Type.mechanical;
                float pr =  Float.parseFloat(ggplot2[2]) ;
                int rp = Integer.parseInt(ggplot2[3]);
                Manufacturer comon = new Manufacturer(ggplot2[4],ggplot2[5]);
                Watch www = new Watch(mark,t,pr,rp,comon);
                addWatches(www);
            }
        }

        catch (IOException e)
        {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        finally
        {
            if (null != reader)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace(System.err);
                }
            }
        }

    }
}


