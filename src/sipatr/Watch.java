package sipatr;
/**
 * Created by Сергей on 28.09.2016.
 */
public class Watch {
    enum Type {
        quartz , mechanical
    }
    public String mark;
    public Type  type;
    public float  price;
    public Integer count;
    public Manufacturer manufacturer;
    public Watch(String mark,Type type,Float price,Integer count,Manufacturer manufacturer){
        this.mark = mark;
        this.type = type;
        this.price = price;
        this.count = count;
        this.manufacturer = manufacturer;
    }
    public String toString (){
        String str = "";
        str =  "mark:    " + mark;
        str += " \nprice:  " + price;
        str += " \ntype:   "  + type;
        str += " \ncount:  " + count;
        str += " \nmanufacturer" + manufacturer;
        return str;

    };
}
