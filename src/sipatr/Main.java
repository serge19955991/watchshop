package sipatr;
//import

import csv.WordsDictionary;
import films.FilmsCollection;
import films.FilmsDef;
import sun.nio.ch.FileKey;
import sun.plugin.javascript.navig.Array;

import javax.lang.model.element.TypeElement;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0){
            args = new String[1];
            args[0] = "text.txt";
        }

        System.out.println("-----------------------\n#1\n-----------------------");

        Manufacturer swissman  = new Manufacturer("Elon Mask","USA");
        Manufacturer usaman = new Manufacturer("Hilary Clinton","Gonduras");
        Manufacturer swissman1  = new Manufacturer("Elon Mask","UK");

        Shop raddisonblue = new Shop();

        Watch expencieve = new Watch("Swiss", Watch.Type.quartz,1234.31f,12,swissman);
        Watch expencievesquare = new Watch("Abby", Watch.Type.quartz,1234.31f,12,swissman1);
        Watch coolwatch = new Watch("Casio", Watch.Type.mechanical,1000f,13,usaman);
        Watch coolwatch1 = new Watch("Gstar", Watch.Type.mechanical,1500f,13,usaman);
        Watch coolwatch2 = new Watch("Chinawatch", Watch.Type.mechanical,2000f,13,usaman);

        raddisonblue.addWatches(expencieve);
        raddisonblue.addWatches(expencievesquare);
        raddisonblue.addWatches(coolwatch);
        raddisonblue.addWatches(coolwatch1);
        raddisonblue.addWatches(coolwatch2);

        raddisonblue.export("watch_shop.txt");

        raddisonblue = new Shop();

        raddisonblue._import("watch_shop.txt");


        System.out.println(" swissman == usaman:    " + swissman.equals(usaman));
        System.out.println(" swissman == swissman1: " + swissman.equals(swissman1));


        raddisonblue.printmarks(Watch.Type.mechanical);
        raddisonblue.printmarks(Watch.Type.quartz);

        raddisonblue.printMechPriceLess(1800);
        raddisonblue.printWatchMarkInSomeCountry("UK");
        raddisonblue.printManufaturerSum(1254);

        for (Watch everest : raddisonblue.watches
             ) {
            System.out.println(everest);
        }

        System.out.println("-----------------------\n#2\n-----------------------");


        FilmsDef Prime = new FilmsDef("Hitman","Lucas",1993,125,"WarnerBros",123456789f);
        FilmsDef Amazon = new FilmsDef("Silent Hill","Adams",2010,115,"Marvel",12312123f);
        FilmsDef eLine = new FilmsDef("The Doors","Valery",2011,80,"Marvel",123129993f);



        FilmsCollection library = new FilmsCollection();
        library.readCSV("films.txt");
        library.addFilm(Prime);
        library.addFilm(Amazon);
        library.addFilm(eLine);
        String stroka = library.afterYearToString(2008);
        System.out.println(stroka) ;



        System.out.println("-----------------------\n#3\n-----------------------");


        WordsDictionary wd = new WordsDictionary();
        wd._import("text.txt");
        wd.wordscount();
        wd.createsortList();
        wd.export("wordscount.csv");

        wd.print();

        new java.util.Scanner(System.in).nextLine();

    }

}
