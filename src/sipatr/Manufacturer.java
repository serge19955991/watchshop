package sipatr;

import com.sun.org.apache.xpath.internal.operations.Equals;

/**
 * Created by Сергей on 28.09.2016.
 */
    public class Manufacturer {
    public String name;
    public String country;
    public Manufacturer(String name,String country){
        this.name = name;
        this.country = country;
    }
    public String toString () {
        return " name : " + name + "\ncountry:" + country;
    }

    public int hashCode()
    {
        return  name.hashCode();
        }

    public boolean equals(Object manufacturer)
    {
        return this.hashCode() == manufacturer.hashCode();
    }
};
