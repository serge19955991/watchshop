package films;

import com.sun.javafx.scene.control.skin.IntegerFieldSkin;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Сергей on 30.09.2016.
 */
public class FilmsCollection {
    Set<FilmsDef> films = new HashSet<>();

    public void addFilm(FilmsDef thefilm) {
        films.add(thefilm);
    }

    public String toString() {
        String str = "Films collectin \n";
        for (FilmsDef thefilm : films) {
            str += thefilm.toString();
        }
        return  str;
    }
    public String afterYearToString(Integer year) {
         String str = "\n\n\nFilms after year " + year + ":\n\n";
        for (FilmsDef temp:films)
        {
            if (temp.getyear()> year)
                str += temp + "\n\n";
        }

        return str;
    }
    public void readCSV (String file)
    {
        Reader reader = null;
        try
        {
            reader = new InputStreamReader(new FileInputStream(file));
            int i;
            String data = "";
            reader = new InputStreamReader(new FileInputStream(file));
            while ( (i=reader.read()) != -1)
                data += (char)i;
            String[] aspen = data.split("\r\n");
            for (String line: aspen)
            {
                  String [] fields = line.split(",");
                  String filename  =  fields[0];
                  String director = fields[1];
                  Integer year = Integer.parseInt(fields[2]) ;
                  Integer  dur = Integer.parseInt(fields[3]);
                  String prod = fields[4];
                  Float budg = Float.parseFloat(fields[5]);
                  FilmsDef f = new FilmsDef(filename, director,year,dur, prod,budg);
                  this.addFilm(f);
            }
        }
        catch (IOException e)
        {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        finally
        {
            if (null != reader)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace(System.err);
                }
            }
        }

    }
}
