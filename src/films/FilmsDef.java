package films;

/**
 * Created by Сергей on 30.09.2016.
 */
public class FilmsDef {
    private String name ;
    private String director;
    private Integer year  ;
    private Integer duration;
    private String Production;
    private Float budget;
    public FilmsDef (String name,String director,Integer year,Integer duration,String Production,Float budget ){
        this.name = name;
        this.director = director;
        this.year  = year;
        this.duration = duration;
        this.Production = Production;
        this.budget = budget;
    }
    public String toString () {
        String str;
        str = "Film " + name + "\n";
        str += "Dir. " + director + "\n";
        str += "BirthYear " + year + "\n";
        str += "Duration " + duration + "\n";
        str += "Production " + Production + "\n";
        str += "Budget " + budget + "\n";
        return str;
    }
    public Integer  getyear (){
        return year;
    }
}
