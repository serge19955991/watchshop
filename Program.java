package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import task1.Fishers;
import task2.Watches;
import task2.WatchesType;
import task3.Words;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Max
 */
public class Program {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Task 1
        Fishers.Execute();
        
        //Task 2
        WatchesExecute();
           
        //Task 3
        Words.Execute();
    }
    
    private static void WatchesExecute(){
        System.out.println("Task 2. Watches.");
        ArrayList<Watches> watches = new ArrayList<>();
        watches.add(new Watches("Citizen A510", WatchesType.Mechanical, 700, 10, "Citizen co.", "Japan"));
        watches.add(new Watches("Rolex CC130", WatchesType.Quartz, 3000, 5, "Rolex inc.", "Swiss"));
        watches.add(new Watches("Omega X", WatchesType.Quartz, 2400, 7, "Omega t.m.", "Swiss"));
        watches.add(new Watches("Konami Sarine", WatchesType.Mechanical, 100, 23, "Citizen co.", "China"));
        //SubTask1
           WatchesType wt = WatchesType.Quartz;
           
           Set<Watches> quartz = new HashSet();
           Set<Watches> mechanical = new HashSet();
           quartz.addAll(watches.stream().filter((watch) -> (watch.Type == WatchesType.Quartz)).collect(Collectors.toList()));
           mechanical.addAll(watches.stream().filter((watch) -> (watch.Type == WatchesType.Mechanical)).collect(Collectors.toList()));
           System.out.println("\tSubTask 1. Brands of "+ wt.name()+" type:");
           
           if(wt == WatchesType.Quartz){
               quartz.stream().forEach((w) -> {
                   System.out.println("\t\t" +  w.Model);
            });
           }else{
               mechanical.stream().forEach((w) -> {
                   System.out.println("\t\t" +  w.Model);
            });
           }
        //SubTask2
           int maxCost = 2500;
           TreeSet<Watches> cheaper = new TreeSet(new Sort());
           cheaper.addAll(watches);
           cheaper = (TreeSet<Watches>) cheaper.headSet(new Watches(null,null, maxCost, 0, null, null), true);
           
           System.out.println("\tSubTask 2. Watches cheaper than $"+ maxCost +" :");
           
           for(Watches w: cheaper){ System.out.println("\t\t" +  w.Model); }
        //SubTask3
           String country = "Swiss";
           HashMap<String, ArrayList<Watches>> countries = new HashMap();
           
           for(Watches w: watches){
               if(!countries.containsKey(w.Producer.Country)){
                  countries.put(w.Producer.Country, new ArrayList());
               }
               countries.get(w.Producer.Country).add(w);
           }
           
           System.out.println("\tSubTask 3. Watches made in "+ country +":");
           for(Watches w: countries.get(country)){
               System.out.println("\t\t" +  w.Model);
           }
        //SubTask4
           int sumCost = 10000;
           System.out.println("\tSubTask 4. Producers who's all watches cheaper than $"+ sumCost +":");
           Map<String, Integer> producers = new HashMap<>();
           watches.stream().forEach((w) -> {
               if(!producers.containsKey(w.Producer.Name)){
                   producers.put(w.Producer.Name, w.Price * w.Count);
               } else{
                   int currCost = producers.get(w.Producer.Name);
                   producers.put(w.Producer.Name, currCost + w.Price * w.Count);
               }
            });
           TreeSet<Entry<String, Integer>> sortedProds = new TreeSet(new ProdComp());
           sortedProds.addAll(producers.entrySet());
           sortedProds = (TreeSet<Entry<String, Integer>>) sortedProds.headSet(new Entry<String, Integer>() {

            @Override
            public String getKey() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Integer getValue() {
                return sumCost;
            }

            @Override
            public Integer setValue(Integer v) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        }, true);
           
           for(Entry<String, Integer> e: sortedProds){
               System.out.println("\t\t" + e.getKey());
           }
    }
}
class Sort implements Comparator<Watches> {
    @Override
    public int compare(Watches a, Watches b) {
        return (b.Price > a.Price) ? -1 : 1;
    }
}
class ProdComp implements Comparator<Entry<String, Integer>> {
    @Override
    public int compare(Entry<String, Integer> a, Entry<String, Integer> b) {
        return (b.getValue() > a.getValue()) ? -1 : 1;
    }
}