/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task3;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Max
 */
public class Words {
    
    public static void Execute(){
        Map<String, Integer> wordMap = new HashMap<>();
        StringBuilder[] words = Getwords();
        
        for(StringBuilder w: words){
            Integer n = wordMap.get(w.toString());
            n = (n == null) ? 1 : ++n;
            wordMap.put(w.toString(), n);
        }
        int all = 0;
        for(Integer val: wordMap.values()) all+= val;
        
        TreeSet<Entry<String, Integer>> sortedWords = new TreeSet(new WordComp());
        sortedWords.addAll(wordMap.entrySet());  
        
        Output(sortedWords, all);
    }
    private static StringBuilder[] Getwords(){
        Reader reader = null;
        ArrayList<StringBuilder> words = new ArrayList<>(); int c, i = 0; boolean flag = false;
        try
        {
            reader = new InputStreamReader(new FileInputStream("C:\\Users\\Max\\Desktop\\test.txt"));
            while((c=reader.read())!=-1){
             
                if(Character.isLetterOrDigit(c)){
                    try {
                        words.get(i).append((char) c);
                    } catch (Exception e) {
                        words.add(new StringBuilder());
                        words.get(i).append((char) c);
                    }
                    flag = true;
                }
                else{
                    if(flag){i++; flag = false;}
                }
             }
        }
        catch (IOException e)
        {
                System.err.println("Error while reading file: " + e.getLocalizedMessage()); 
        }
        finally
        {
                if (null != reader)
                {
                        try
                        {
                                reader.close();
                        }
                        catch (IOException e)
                        {
                                e.printStackTrace(System.err);
                        }
                }
        }
        return words.toArray(new StringBuilder[words.size()]);
    }
    
    private static void Output(TreeSet<Entry<String, Integer>> sortedWords, int all){
        Writer writer = null;
        try
        {
            writer = new OutputStreamWriter(new FileOutputStream("C:\\Users\\Max\\Desktop\\out.txt"));
            
            for(Entry e: sortedWords){
                String separator = System.getProperty("line.separator");
                double freq = ((int)e.getValue()/(double)all)*100;
                writer.write(e.getKey()+", "+e.getValue()+", "+freq);
                writer.append(separator);
            }
        }
        catch (IOException e)
        {
                System.err.println("Error while writing file: " + e.getLocalizedMessage()); 
        }
        finally
        {
                if (null != writer)
                {
                        try
                        {
                                writer.close();
                        }
                        catch (IOException e)
                        {
                                e.printStackTrace(System.err);
                        }
                }
        }
    }
}
class WordComp implements Comparator<Entry<String, Integer>> {
    @Override
    public int compare(Entry<String, Integer> a, Entry<String, Integer> b) {
        return (b.getValue() > a.getValue()) ? 1 : -1;
    }
}
